﻿using System.Collections.Generic;

namespace contactList.Model
{
    public class Contact
    {
        public Contact()
        {
            ContactInformations = new List<ContactInformation>();
            Tags = new List<Tag>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public bool Favorite { get; set; }

        public virtual ICollection<ContactInformation> ContactInformations { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
