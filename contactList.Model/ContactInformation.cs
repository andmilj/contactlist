﻿using System;

namespace contactList.Model
{
    public enum ContactType
    {
        Email,
        Phone
    }
    public class ContactInformation
    {
        public int Id { get; set; }
        public ContactType Type { get; set; }
        public string Value { get; set; }

        public Nullable<int> ContactId { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
