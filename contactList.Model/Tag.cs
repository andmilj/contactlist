﻿using System.Collections.Generic;

namespace contactList.Model
{
    public class Tag
    {
        public Tag()
        {
            Contacts = new List<Contact>();
        }
        public int Id { get; set; }
        public string TagName { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
