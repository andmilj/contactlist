﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using contactList.Repository.Interfaces;

namespace contactList.Repository
{
    public class Base<T> : IBase<T>
        where T : class
    {

        public Base()
        {
            DataContext = new Context();
            DbSet = DataContext.Set<T>();
            DataContext.Configuration.ProxyCreationEnabled = false;
            DataContext.Configuration.LazyLoadingEnabled = false;
        }

        public Base(Context context)
        {
            DataContext = context;
            DbSet = DataContext.Set<T>();
            DataContext.Configuration.ProxyCreationEnabled = false;
            DataContext.Configuration.LazyLoadingEnabled = false;
        }

        public Context DataContext { get; set; }

        public DbSet<T> DbSet { get; set; }

        public virtual IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public virtual T GetById(int id)
        {
            return DbSet.Find(id);
        }

        public virtual void Update(T entity)
        {
            DataContext.Set<T>().Attach(entity);
            DataContext.Entry(entity).State = EntityState.Modified;
            DataContext.SaveChanges();
        }

        public void Delete(T entity)
        {

            DbEntityEntry dbEntityEntry = DataContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
            DataContext.SaveChanges();
        }

        public void Delete(int id)
        {

            var entity = GetById(id);
            Delete(entity);
        }

        public virtual void Add(T entity)
        {

            DbSet.Add(entity);
            DataContext.SaveChanges();

        }

        public virtual void Dispose()
        {
            if (DataContext != null)
                DataContext.Dispose();
        }
    }
}
