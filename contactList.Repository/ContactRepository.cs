﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using contactList.Model;
using contactList.Repository.Interfaces;

namespace contactList.Repository
{
    public class ContactRepository : Base<Contact>, IContactRepository
    {
        public override IQueryable<Contact> GetAll()
        {
            return DbSet.Include(c => c.ContactInformations).Include(c => c.Tags);
        }

        public IQueryable<Contact> GetAllFiltered(String name = "", String surname = "", String tagname = "")
        {
            return GetAll()
                .Where(c => c.Name.Contains(name) && c.Surname.Contains(surname)
                            && (c.Tags.Any(tag => tag.TagName.Contains(tagname)) || tagname == ""));
        }

        public Contact GetByIdWithTags(int id)
        {
            return GetAll().FirstOrDefault(c => c.Id == id);
        }

        public bool RemoveTagsByTagId(int tagId, int id)
        {
            var contact = GetByIdWithTags(id);
            var tag = contact.Tags.FirstOrDefault(t => t.Id == tagId);
            contact.Tags.Remove(tag);

            return DataContext.SaveChanges() > 0;
        }

        public Tag AddTag(String name, int id)
        {
            var tag = DataContext.Tags.FirstOrDefault(c => c.TagName.Equals(name));
            if (tag == null)
            {
                tag = new Tag
                {
                    Id = 0,
                    TagName = name
                };
                DataContext.Tags.Add(tag);
            }
            var contact = GetByIdWithTags(id);
            contact.Tags.Add(tag);

            return DataContext.SaveChanges() > 0 ? tag : null;
        }
    }
}
