﻿using System.Data.Entity;
using contactList.Model;

namespace contactList.Repository
{
    public class Context: DbContext
    {
        public Context() : base("DefaultConnection")
        {
        }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactInformation> ContactInformations { get; set; }
        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                    .HasMany(a => a.ContactInformations)
                    .WithRequired(b => b.Contact)
                    .WillCascadeOnDelete();
        }
    }
}
