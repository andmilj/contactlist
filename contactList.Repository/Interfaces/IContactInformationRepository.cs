﻿using contactList.Model;

namespace contactList.Repository.Interfaces
{
    public interface IContactInformationRepository : IBase<ContactInformation>
    {
    }
}
