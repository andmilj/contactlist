﻿using System;
using System.Linq;
using contactList.Model;

namespace contactList.Repository.Interfaces
{
    public interface IContactRepository : IBase<Contact>
    {
        bool RemoveTagsByTagId(int id, int tagId);
        Contact GetByIdWithTags(int id);
        Tag AddTag(String tag, int id);
        IQueryable<Contact> GetAllFiltered(String name = "", String surname = "", String tagname = "");
    }
}
