﻿using contactList.Model;

namespace contactList.Repository.Interfaces
{
    public interface ITagRepository : IBase<Tag>
    {
    }
}
