﻿using System.Data.Entity.Migrations;
using contactList.Model;

namespace contactList.Repository.Migration
{
    class Configuration : DbMigrationsConfiguration<Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Context context)
        {
            for (int i = 1; i < 30; i++)
            {
                Contact contact = new Contact { Id = i, 
                    Name = "Andrej" + i, 
                    Surname = "Miljus", 
                    Address = "Adresa " + i ,
                    Favorite = false };

                contact.Tags.Add(new Tag{TagName = "lalala"+i});
                context.Contacts.AddOrUpdate(p => p.Name,
                contact);
            }
            
            context.Tags.AddOrUpdate(p=>p.TagName,
                new Tag{TagName = "abs"});

            context.ContactInformations.AddOrUpdate(p => p.Value,
                new ContactInformation { Type = ContactType.Email, ContactId = 1, Value = "andrej.miljus@gmail.com" });
            context.SaveChanges();
        }
    }
}
