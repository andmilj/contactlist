﻿using System.Web.Mvc;
using contactList.Repository;
using contactList.Repository.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;

namespace contactList
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            container.RegisterType<IContactRepository, ContactRepository>();
            container.RegisterType<IContactInformationRepository, ContactInformationRepository>();
            container.RegisterType<ITagRepository, TagRepository>();
            // e.g. container.RegisterType<ITestService, TestService>();            

            return container;
        }
    }
}