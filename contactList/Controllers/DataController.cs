﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using contactList.Model;
using contactList.Repository.Interfaces;

namespace contactList.Controllers
{
    public class DataController : Controller
    {
        private IContactRepository _contactRepository;
        private IContactInformationRepository _contactInformationRepository;

        public DataController(IContactRepository contactRepository,
            IContactInformationRepository contactInformationRepository)
        {
            _contactRepository = contactRepository;
            _contactInformationRepository = contactInformationRepository;
        }

        #region Contact CRUD
        [HttpGet]
        public JsonResult GetContacts(String name = "", String surname = "", String tagname = "")
        {
            var contacts = _contactRepository.GetAllFiltered(name, surname, tagname)
                .ToList().Select(c => new
                    {
                        c.Address,
                        c.Name,
                        c.Surname,
                        c.Id,
                        c.Favorite,
                       ContactInformations = c.ContactInformations.ToList().Select(ci=>new
                        {
                            ci.Id,
                            ci.Type,
                            ci.Value,
                            ci.ContactId
                        }),
                        Tags = c.Tags.ToList().Select(t => new
                        {
                            t.Id,
                            t.TagName
                        })
                    });
            return Json(contacts, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetContact(int id)
        {
            return Json(_contactRepository.GetById(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddContact(Contact contact)
        {
            _contactRepository.Add(contact);
            return Json(contact);
        }

        [HttpPost]
        public JsonResult DeleteContact(int id)
        {
            _contactRepository.Delete(id);
            return Json(id);
        }

        [HttpPost]
        public JsonResult UpdateContact(Contact contact)
        {
            _contactRepository.Update(contact);
            return Json(contact);
        }

        [HttpPost]
        public JsonResult FavoriteContact(int id, bool favorite)
        {
            var contact = _contactRepository.GetById(id);
            contact.Favorite = favorite;
            _contactRepository.Update(contact);
            return Json(contact);
        }
        #endregion

        #region ContactInformation CRUD
        [HttpPost]
        public JsonResult AddContactInformation(ContactInformation contact)
        {
            _contactInformationRepository.Add(contact);
            return Json(contact);
        }

        [HttpPost]
        public JsonResult DeleteContactInformation(int id)
        {
            _contactInformationRepository.Delete(id);
            return Json(id);
        }
        #endregion

        [HttpPost]
        public JsonResult DeleteTag(int id, int contactId)
        {
            return Json(_contactRepository.RemoveTagsByTagId(id, contactId));
        }

        [HttpPost]
        public JsonResult AddTag(String name, int contactId)
        {
            var tag = _contactRepository.AddTag(name, contactId);
            if (tag != null)
            {
                tag.Contacts = null;
            }
            return Json(tag);
        }
    }
}