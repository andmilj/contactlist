﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using contactList.Repository.Interfaces;

namespace contactList.Controllers
{
    public class HomeController : Controller
    {
        private IContactRepository _contactRepository;

        public HomeController(IContactRepository cRepo)
        {
            _contactRepository = cRepo;
        }
        public ActionResult Index()
        {         
            var list = _contactRepository.GetAll().ToList();
            return View(list);
        }
    }
}