﻿var contactListApp = angular.module("contactListApp", [ "ui.bootstrap", "ngRoute"])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "/ngViews/index.html",
                controller: "homeController",
                resolve: {
                    contacts: function (contactDataService) {
                        return contactDataService.getContacts();
                    }
                }
            })
            .when("/add", {
                templateUrl: "/ngViews/contact.html",
                controller: "contactController",
                resolve: {
                    contact: function (contactDataService) {
                        return contactDataService.getEmptyContact();
                    }
                }
            })
            .when("/update/:id", {
                templateUrl: "/ngViews/contact.html",
                controller: "contactController",
                resolve: {
                    contact: function (contactDataService, $route) {
                        return contactDataService.getContact($route.current.params.id);
                    }
                }
            })
            .otherwise({
                redirectTo: "/"
            });
    });