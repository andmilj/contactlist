﻿contactListApp
    .controller("contactController", function ($scope, $location, contactDataService, contact) {
        $scope.contact = contact;
        $scope.addContact = function (addForm, _contact) {
            if (addForm.$valid) {
                contactDataService.addContact(_contact).then(function () {
                    $location.path("/");
                });
            }
        };
        $scope.updateContact = function (addForm, _contact) {
            if (addForm.$valid) {
                contactDataService.updateContact(_contact).then(function () {
                    $location.path("/");
                });
            }
        };
    });