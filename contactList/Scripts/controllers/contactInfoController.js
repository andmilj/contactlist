﻿contactListApp
    .controller("contactInfoController", function ($scope, $modalInstance,
        contactInfos, contactId, contactInformationDataService) {
        $scope.contactInfos = contactInfos;
        $scope.options = [
        {
            id: 0,
            name: "EMAIL"
        },
        {
            id: 1,
            name: "PHONE"
        }];

        $scope.contactInfo = {
            Id: 0,
            ContactId: contactId,
            Type: null,
            Value: null
        };

        $scope.add = function () {
            if ($scope.contactInfo.ContactId
                && $scope.contactInfo.Value) {
                contactInformationDataService.add($scope.contactInfo)
                    .then(function (data) {
                        $scope.contactInfo.Type = 0;
                        $scope.contactInfo.Value = null;
                        $scope.contactInfos.unshift(data);
                    });
            }
        };
        $scope.remove = function (contactInfoId, index) {
            contactInformationDataService.remove(contactInfoId).then(function () {
                $scope.contactInfos.splice(index, 1);
            });
        };
        $scope.close = function () {
            $modalInstance.dismiss("cancel");
        };
    });