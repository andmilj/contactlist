﻿contactListApp
    .controller("homeController", function ($scope, contacts, contactDataService,
        $location, $modal) {
        $scope.name = "home";
        $scope.tags = [];

        $scope.contacts = contacts;
        $scope.filter = {
            name: "",
            surname: "",
            tagname: ""
        };

        $scope.$watch("filter", function (newValue, oldValue) {
            if (newValue !== oldValue) {
                contactDataService.getContacts($scope.filter).then(function (data) {
                    $scope.contacts = data;
                });
            }
        }, true);

        $scope.deleteContact = function (id, index) {
            contactDataService.deleteContact(id).then(function () {
                $scope.contacts.splice(index, 1);
            });
        }

        $scope.goToEditPage = function (id) {
            $location.path("/update/" + id);
        }

        $scope.seeInformations = function (contactInfos, contactId) {
            $modal.open({
                animation: true,
                templateUrl: "/ngViews/informations.html",
                controller: "contactInfoController",
                size: "lg",
                resolve: {
                    contactInfos: function () {
                        return contactInfos;
                    },
                    contactId: function () {
                        return contactId;
                    }
                }
            });
        };

        $scope.favorite = function (id, status, index) {
            contactDataService.favoriteContact(id, !status).then(function(contact) {
                $scope.contacts[index].Favorite = contact.Favorite;
            });
        }
    });