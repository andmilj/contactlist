﻿contactListApp.directive("tagger", function (contactDataService) {
    return {
        restrict: "E",
        scope: {
            tags: "=",
            contactid: "="
        },
        templateUrl: "/ngViews/tagger.html",
        link: function ($scope) {
            $scope.add = function () {
                var newTagName = $scope.new_value;
                if (newTagName) {
                    var tags = $scope.tags.filter(function(obj) {
                        return obj.TagName === newTagName;
                    });
                    if (tags.length === 0) {
                        contactDataService.addTag(newTagName, $scope.contactid)
                            .then(function(tag) {
                                if (tag) {
                                    $scope.tags.push({
                                        Id:tag.Id,
                                        TagName: tag.TagName
                                    });
                                }
                            });
                        
                    }
                }
                $scope.new_value = "";
            };
            $scope.remove = function (idx) {
                contactDataService.deleteTag($scope.tags[idx].Id, $scope.contactid)
                    .then(function (success) {
                        if (success) {
                            $scope.tags.splice(idx, 1);
                        };
                    });
            };
            $scope.onKeyPress = function (event) {
                if (event.keyCode === 13) {
                    $scope.add();
                }
            }
        }
    };
});
