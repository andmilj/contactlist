﻿contactListApp.filter("contactTypeToText", function () {
    return function (input, uppercase) {
        var result = "";
        switch (input) {
            case 0:
                result = "email";
                break;
            case 1:
                result = "phone";
                break;
            default:
                result = "unknown";
                break;
        }
        if (uppercase) {
            result = result.toUpperCase();
        }
        return result;
    }
});