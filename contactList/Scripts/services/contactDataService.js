﻿contactListApp.factory("contactDataService", [
    "$http", "$q", function($http, $q) {
        var _getContacts = function(filter) {
                var deferred = $q.defer();

                $http({
                    url: "Data/GetContacts",
                    method: "GET",
                    params: filter
                }).then(function(result) {
                        deferred.resolve(result.data);
                    },
                    function() {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            _addContact = function(contact) {

                var deferred = $q.defer();

                $http.post("Data/AddContact", contact).then(function(result) {
                        deferred.resolve(result.data);
                    },
                    function() {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            _deleteContact = function(id) {
                var deferred = $q.defer();

                $http.post("Data/DeleteContact", { id: id }).then(function(result) {
                        deferred.resolve(result.data);
                    },
                    function() {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            _getContact = function(id) {
                var deferred = $q.defer();

                $http.get("Data/GetContact/" + id).then(function(result) {
                        deferred.resolve(result.data);
                    },
                    function() {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            _updateContact = function(contact) {
                var deferred = $q.defer();

                $http.post("Data/UpdateContact", contact).then(function(result) {
                        deferred.resolve(result.data);
                    },
                    function() {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            _getEmptyContact = function() {
                var deferred = $q.defer();
                deferred.resolve({
                    Name: "",
                    Address: "",
                    Surname: ""
                });
                return deferred.promise;
            },
            _deleteTag = function(id, contactId) {
                var deferred = $q.defer();

                $http.post("Data/DeleteTag", {id:id, contactId:contactId}).then(function (result) {
                    deferred.resolve(result.data);
                },
                    function () {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            _addTag = function (name, contactId) {
                var deferred = $q.defer();

                $http.post("Data/AddTag", { name: name, contactId: contactId }).then(function (result) {
                    deferred.resolve(result.data);
                },
                    function () {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            _favoriteContact = function (id, favorite) {
                var deferred = $q.defer();

                $http.post("Data/FavoriteContact", { id: id, favorite: favorite }).then(function (result) {
                    deferred.resolve(result.data);
                },
                    function () {
                        deferred.reject();
                    });

                return deferred.promise;
            };

        return {
            getContacts: _getContacts,
            addContact: _addContact,
            deleteContact: _deleteContact,
            getContact: _getContact,
            getEmptyContact: _getEmptyContact,
            updateContact: _updateContact,
            deleteTag: _deleteTag,
            addTag: _addTag,
            favoriteContact: _favoriteContact
        };
    }
]);