﻿contactListApp.factory("contactInformationDataService", function ($http, $q) {
    var _addContactInfromation = function(contactInfo) {

            var deferred = $q.defer();

            $http.post("Data/AddContactInformation", contactInfo).then(function (result) {
                    deferred.resolve(result.data);
                },
                function() {
                    deferred.reject();
                });

            return deferred.promise;
        },
        _deleteContactContactInformation = function(id) {
            var deferred = $q.defer();

            $http.post("Data/DeleteContactInformation", { id: id }).then(function(result) {
                    deferred.resolve(result.data);
                },
                function() {
                    deferred.reject();
                });

            return deferred.promise;
        };
   

    return {
        remove: _deleteContactContactInformation,
        add: _addContactInfromation
    };
});